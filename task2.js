const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

function sortObj (arr) {
  const result = [];
  for (let index in arr) {
    let obj = arr[index];
    let {name , lastName , age} = obj;
    const persone = {
      name: name,
      lastName: lastName,
      age:age,
    }
    result.push(persone)
  }
return result;
}
const charactersShortInfoResult = sortObj(characters);



const charactersShortInfoResult2 = sortObj2(characters);

console.log('charactersShortInfoResult ->', charactersShortInfoResult);
console.log('charactersShortInfoResult2 ->', charactersShortInfoResult2)

function sortObj2(charactersInput) {
  return charactersInput.map(character => {
    const { name, lastName, age } = character;
    return {
      name,
      lastName,
      age
    };
  })
}