const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
  const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
  const clients = [...clients1, ...clients2];
  
  function unique (arr) {
    let result = [];
    for (let str of arr) {
      if(!result.includes(str)) {
        result.push (str)
      }
    }
    return result;
  }
  let res =  unique(clients);
  console.log(res)