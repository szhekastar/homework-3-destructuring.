const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}


const arrObj = [satoshi2018,satoshi2019,satoshi2020 ];

function parseSatoshiData (arr) {
  const result = {};
  arr.forEach(element => {
    const {name ,surname,age,country,birth,location,wallet,browser,technology} = element;
    result['name'] = name || result.name;
    result['surname'] = surname || result.surname;
    result['age'] = age || result.age;
    result['country'] = country || result.country;
    result['birth'] = birth || result.birth;
    result['location'] = location || result.location;
    result['wallet'] = wallet || result.wallet;
    result['browser'] = browser || result.browser;
    result['technology'] = technology || result.technology;
  });
  return result;
}

let fullProfile = parseSatoshiData(arrObj);
console.log(fullProfile);

console.log(' ->', {...satoshi2018, ...satoshi2019, ...satoshi2020});
